﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Todo.Entities
{
    public enum PriorityValue
    {
        Critical = 0,
        High = 1,
        Low = 2,
        Minimum = 3
    }
}
