﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;


namespace Todo.Repositories
{
    public class SqlTaskRepository : ITaskRepository
    {
        private string _connectionString;
        private string _querySelectAllTask = "SELECT [Id],[Title],[IsDone],[Priority] FROM tblTasks";
        private string _querySelectTasksByUserId = "SELECT [Id],[UserId],[Title],[IsDone],[Priority] FROM tblTasks WHERE [UserId] = @id";
        private string _querySelectById = "SELECT [Id],[UserId],[Title],[IsDone],[Priority] FROM tblTasks WHERE [Id] = ";
        private string _queryInsertOneTask = @"INSERT INTO tblTasks([UserId], [Title], [IsDone],[Priority]) VALUES(@userId, @title, @isDone, @priority)";
        private string _queryDeleteTaskById = "DELETE FROM tblTasks WHERE [Id] = ";
        private string _queryUpdateTaskById = "UPDATE tblTasks SET [Title] = @title, [IsDone] = @isDone, [Priority] = @priority WHERE [Id] = @id";
        private string _queryUpdateStatusTaskById = "UPDATE tblTasks SET [IsDone] = @isDone WHERE [Id] = @id";
        private string _querySelectUserIdByEmail = "SELECT[Id] FROM tblUsers WHERE [Email] = @email";

        public SqlTaskRepository(string stringConnection)
        {
            _connectionString = stringConnection;
        }

        public void AddTask(string textTask, int userId)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection())
                {
                    connection.ConnectionString = this._connectionString;
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(_queryInsertOneTask, connection))
                    {
                        command.Parameters.Add("@userId", SqlDbType.Int).Value = userId;
                        command.Parameters.Add("@title", SqlDbType.NVarChar, 255).Value = textTask;
                        command.Parameters.Add("@isDone", SqlDbType.Bit, 255).Value = false;
                        command.Parameters.Add("@priority", SqlDbType.Int, 255).Value = 3;
                        command.ExecuteNonQuery();
                    }
                    connection.Close();
                }
            }
            catch
            {
                throw new Exception("Inccorect data");
            }
        }

        public void DeleteTask(int idTask)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(String.Format("{0}{1}", _queryDeleteTaskById, idTask), connection))
                {
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
        }

        public List<TaskEntity> SelectAll()
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_querySelectAllTask, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<TaskEntity> selectedTask = new List<TaskEntity>();
                        while(reader.Read())
                        {
                            selectedTask.Add(new TaskEntity() {
                                Id = (int)reader["Id"],
                                UserId = (int)reader["UserId"],
                                Title = (string)reader["Title"],
                                IsDone = (bool)reader["IsDone"],
                                Priority = (PriorityValue)reader["Priority"] 
                            });
                        }
                        return selectedTask;
                    }
                }
            }
        }

        public TaskEntity SelectById(int id)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand( String.Format("{0}{1}", _querySelectById, id), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        TaskEntity selectedTask = new TaskEntity();
                        reader.Read();
                        selectedTask.Id = (int)reader["Id"];
                        selectedTask.UserId = (int)reader["UserId"];
                        selectedTask.Title = (string)reader["Title"];
                        selectedTask.IsDone = (bool)reader["IsDone"];
                        selectedTask.Priority = (PriorityValue)reader["Priority"];
                        return selectedTask;
                    }
                }
            }
        }

        public void UpdateTask(int id, string title, bool isDone, PriorityValue priority)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_queryUpdateTaskById, connection))
                {
                    command.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@title", SqlDbType.NVarChar, 255).Value = title;
                    command.Parameters.Add("@isDone", SqlDbType.Bit).Value = isDone;
                    command.Parameters.Add("@priority", SqlDbType.Int).Value = (int)priority;
                    command.ExecuteNonQuery();
                }
            }
        }
        public void UpdateTaskStatus(int id, bool isDone)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = _connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(_queryUpdateStatusTaskById, connection))
                {
                    command.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    command.Parameters.Add("@isDone", SqlDbType.Bit).Value = isDone;
                    command.ExecuteNonQuery();
                }
            }
        }
        public List<TaskEntity> SelectTasksByUserId(int userId)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_querySelectTasksByUserId, connection))
                {
                    command.Parameters.Add("@id", SqlDbType.Int).Value = userId;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        List<TaskEntity> selectedTask = new List<TaskEntity>();
                        while (reader.Read())
                        {
                            selectedTask.Add(new TaskEntity()
                            {
                                Id = (int)reader["Id"],
                                UserId = (int)reader["UserId"],
                                Title = (string)reader["Title"],
                                IsDone = (bool)reader["IsDone"],
                                Priority = (PriorityValue)reader["Priority"]
                            });
                        }
                        return selectedTask;
                    }
                }
            }
        }

        public int GetUserIdByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();

                using (SqlCommand command = new SqlCommand(_querySelectUserIdByEmail, connection))
                {
                    command.Parameters.Add("@email", SqlDbType.NVarChar, 255).Value = email;
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        int userId;
                        reader.Read();
                        userId = (int)reader["Id"];
                        return userId;
                    }
                }
            }
        }
    }
}
