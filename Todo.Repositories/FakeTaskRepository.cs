﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class FakeTaskRepository : ITaskRepository
    {
        private static List<TaskEntity> _fakeRepository = new List<TaskEntity>();
        private static int CounterId = 0;

        public List<TaskEntity> SelectAll()
        {
            return _fakeRepository;
        }
        public void AddTask(string TextTask, int userId)
        {
            CounterId += 1;
            _fakeRepository.Add(new TaskEntity() {Id = CounterId, Title = TextTask, IsDone = true, Priority = PriorityValue.Minimum });
            
        }
        public void DeleteTask(int idTask)
        {
            _fakeRepository.RemoveAll(t => t.Id == idTask);
        }
        public TaskEntity SelectById(int id)
        {
            return _fakeRepository.Find(t => t.Id == id);
        }
        public void UpdateTask(int id, string title, bool isDone, PriorityValue priority)
        {
            _fakeRepository.First(t => t.Id == id).Title = title;
            _fakeRepository.First(t => t.Id == id).IsDone = isDone;
            _fakeRepository.First(t => t.Id == id).Priority = priority;
        }
        public void UpdateTaskStatus(int id, bool isDone)
        {
            _fakeRepository.First(t => t.Id == id).IsDone = isDone;
        }

        public void SelectById()
        {
            throw new NotImplementedException();
        }

        public List<TaskEntity> SelectTasksByUserId(int userId)
        {
            throw new NotImplementedException();
        }

        public int GetUserIdByEmail(string email)
        {
            throw new NotImplementedException();
        }
    }
}
