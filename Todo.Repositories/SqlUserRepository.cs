﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public class SqlUserRepository : IUserRepository
    {
        private string _connectionString;

        private string _queryGetUserByEmail = "SELECT * FROM tblUsers WHERE [Email] = ";

        public SqlUserRepository(string stringConnection)
        {
            _connectionString = stringConnection;
        }

        public UserEntity GetUserByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection())
            {
                connection.ConnectionString = this._connectionString;
                connection.Open();
                using (SqlCommand command = new SqlCommand(String.Format("{0}'{1}'", _queryGetUserByEmail, email), connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        UserEntity selectedUser = new UserEntity();
                        reader.Read();
                        if (reader.HasRows)
                        {
                            selectedUser.Id = (int)reader["Id"];
                            selectedUser.FirstName = (string)reader["FirstName"];
                            selectedUser.LastName = (string)reader["LastName"];
                            selectedUser.Email = (string)reader["Email"];
                            selectedUser.PhoneNumber = (string)reader["PhoneNumber"];
                            selectedUser.Password = (string)reader["Password"];
                            return selectedUser;
                        }
                        return selectedUser;
                    }
                }
            }
        }
    }
}
