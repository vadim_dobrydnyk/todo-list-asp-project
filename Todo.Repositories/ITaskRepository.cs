﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Todo.Entities;

namespace Todo.Repositories
{
    public interface ITaskRepository
    {
        List<TaskEntity> SelectAll();
        void AddTask(string textTask, int userId);
        void DeleteTask(int idTask);
        TaskEntity SelectById(int id);
        void UpdateTask(int id, string title, bool isDone, PriorityValue priority);
        void UpdateTaskStatus(int id, bool isDone);
        List<TaskEntity> SelectTasksByUserId(int userId);
        int GetUserIdByEmail(string email);
    }
}
