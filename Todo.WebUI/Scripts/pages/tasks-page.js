﻿(function () {
    var onIsDoneClick = function (e) {
        console.log("[tasks-page.js] checkbox click", e.target);
        var ch = e.target;
        var taskId = ch.getAttribute("data-task-id");
        var isCheck = ch.checked;
        
        $.ajax({
            url: "/Task/ChangeStatus",
            type: "POST",
            dataType: "text",
            data: {
                taskId: taskId,
                isDone: isCheck
            }
        }).done(function () {
            console.log("Done");
            changeIsDoneStatus(ch);
        }).fail(function () {
            console.log("Fail");
        });
    };

    var changeIsDoneStatus = function (ch) {
        $(ch).parent().parent().children("[title]").toggleClass("title-is-done");
    };

    var changeFilter = function (e) {
        var filterStatus = $(e.target).children(":selected").attr("value");
        var items = $("table > tbody > tr").get();
        switch (filterStatus) {
            case "all":
                for (var i = 0; i < items.length; i++) {
                    $(items[i]).css("display", "");
                }
                break;
            case "inProgers":
                for (var i = 0; i < items.length; i++) {
                    $(items[i]).children().children("[type='checkbox']").get(0).checked ? $(items[i]).css("display", "none") : $(items[i]).css("display", "");
                }
                break;
            case "done":
                for (var i = 0; i < items.length; i++) {
                    $(items[i]).children().children("[type='checkbox']").get(0).checked ? $(items[i]).css("display", "") : $(items[i]).css("display", "none");
                }
                break;
        }
    };
    var init = function () {
        console.log("[tasks-page.js] init");
        $("input[type='checkbox']").on("click", onIsDoneClick);
        $(".filter").on("change", changeFilter);
    };  
    $(function () {
        console.log("[tasks-page.js] main");
        init();
    })
})();
