﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Todo.Repositories;
using Todo.Entities;
using System.Web.Security;
using System.Web.Mvc;

namespace Todo.WebUI.Code.Managers
{
    public class FormsSecurityManager : ISecurityManager
    {
        private IUserRepository _userRepository;

        public FormsSecurityManager(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public bool Login(string userName, string password)
        {
            UserEntity user = _userRepository.GetUserByEmail(userName);
            if(user.Email == userName && user.Password == password)
            {
                FormsAuthentication.SetAuthCookie(user.Email, false);
                return true;
            }

            return false;
        }
        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}