using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using Todo.Repositories;
using Todo.WebUI.Code.Managers;
using System.Configuration;

namespace Todo.WebUI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            var connectionString = ConfigurationManager.ConnectionStrings["TodoList"].ConnectionString;

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            //container.RegisterType<ITaskRepository, FakeTaskRepository>();
            container.RegisterType<ITaskRepository, SqlTaskRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<IUserRepository, SqlUserRepository>(new InjectionConstructor(connectionString));
            container.RegisterType<ISecurityManager, FormsSecurityManager>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}