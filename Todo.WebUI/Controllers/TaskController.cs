﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.Entities;
using Todo.Repositories;
using Todo.WebUI.Models;
using System.Configuration;
using System.Web.Security;

namespace Todo.WebUI.Controllers
{
    [Authorize]
    public class TaskController : Controller
    {
        private readonly ITaskRepository _taskRepository;

        public TaskController(ITaskRepository repository)
        {
            //this._taskRepository = new SqlTaskRepository(ConfigurationManager.ConnectionStrings["TodoList"].ConnectionString);
            //this._taskRepository = new FakeTaskRepository();
            this._taskRepository = repository;
        }

        [HttpGet]
        public ActionResult Index()
        {
            string CurrentUserName = HttpContext.User.Identity.Name;
            int userId = _taskRepository.GetUserIdByEmail(CurrentUserName);
            ViewBag.Tasks = _taskRepository.SelectTasksByUserId(userId);
            return View();
        }

        [HttpPost]
        public ActionResult Index(string textTask)
        {
            #region Validation
            if(String.IsNullOrWhiteSpace(textTask))
            {
                throw new ArgumentException("Error! The Task can not be empty!");
            }
            if (textTask.Length > 50)
            {
                throw new ArgumentException("Error! The Task can not be more than 10 symbols!");
            }
            #endregion
            string CurrentUserName = HttpContext.User.Identity.Name;
            int userId = _taskRepository.GetUserIdByEmail(CurrentUserName);
            _taskRepository.AddTask(textTask, userId);
            ViewBag.Tasks = _taskRepository.SelectTasksByUserId(userId);
            return RedirectToAction("Index", "Task");
        }
        [HttpPost]
        public ActionResult Delete(int idTask)
        {
            _taskRepository.DeleteTask(idTask);
            return RedirectToAction("Index", "Task");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            TaskEntity task = _taskRepository.SelectById(id);
            TaskModel taskModel = new TaskModel { Id = task.Id,  Title = task.Title, IsDone = task.IsDone};
            return View(taskModel);
        }

        [HttpPost]
        public ActionResult Edit(TaskModel model)
        {
            #region Validation
            if (String.IsNullOrWhiteSpace(model.Title))
            {
                throw new ArgumentException("Error! The Task can not be empty!");
            }
            if (model.Title.Length > 50)
            {
                throw new ArgumentException("Error! The Task can not be more than 10 symbols!");
            }
            #endregion
            _taskRepository.UpdateTask(model.Id, model.Title, model.IsDone, model.Priority);
            return RedirectToAction("Index", "Task");
        }
        [HttpPost]
        public ActionResult ChangeStatus(int taskId, bool isDone)
        {
            _taskRepository.UpdateTaskStatus(taskId, isDone);
            return new EmptyResult();
        }
    }
}