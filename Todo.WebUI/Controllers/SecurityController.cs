﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Todo.WebUI.Code.Managers;
using Todo.WebUI.Models;

namespace Todo.WebUI.Controllers
{
    public class SecurityController : Controller
    {
        private readonly ISecurityManager _securityManger;

        public SecurityController(ISecurityManager securityManger)
        {
            _securityManger = securityManger;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(SecurityModel securityModel)
        {
            if(_securityManger.Login(securityModel.Login, securityModel.Password))
            {
                return RedirectToAction("Index", "Task");
            }
            ViewBag.ErrorString = "Error";
            return View();
        }
        [HttpPost]
        public ActionResult Logout()
        {
            _securityManger.Logout();
            return RedirectToAction("Login", "Security");
        }

    }
}