﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Todo.Entities;

namespace Todo.WebUI.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public string Title { get; set; }
        public bool IsDone { get; set; }
        public PriorityValue Priority { get; set; }
    }
}