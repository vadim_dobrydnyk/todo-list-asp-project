CREATE DATABASE Todo

go 

use Todo

CREATE TABLE tblTasks (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[UserId] int not null,
	[Title] nvarchar(512) null,
	[IsDone] bit not null,
	[Priority] int not null
);

CREATE TABLE tblUsers (
	[Id] int not null identity(1,1) PRIMARY KEY,
	[FirstName] nvarchar(255) not null,
	[LastName] nvarchar(255) not null,
	[Email] nvarchar(255) not null,
	[PhoneNumber] nvarchar(255) null,
	[Password] nvarchar(255) null 
);





