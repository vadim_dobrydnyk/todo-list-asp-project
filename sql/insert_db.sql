use  Todo

SET IDENTITY_INSERT tblTasks ON

INSERT INTO tblTasks
		([Id], [UserId], [Title], [IsDone], [Priority])
	VALUES
		(1, 1, 'Some task 1', 0, 0),
		(2, 2, 'Some task 2', 0, 1),
		(3, 3, 'Some task 3', 1, 2)

SET IDENTITY_INSERT tblTasks OFF


SET IDENTITY_INSERT tblUsers ON

INSERT INTO tblUsers
		([Id], [FirstName], [LastName], [Email], [PhoneNumber], [Password])
	VALUES
		(1, 'name', 'lastName', 'email1@gmail.com', '234567', 'qweqweqwe'),
		(2, 'name', 'lastName', 'email2@gmail.com', '234567', 'qweqweqwe'),
		(3, 'name', 'lastName', 'email3@gmail.com', '234567', 'qweqweqwe')

SET IDENTITY_INSERT tblTasks OFF